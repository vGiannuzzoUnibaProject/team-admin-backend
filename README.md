

# Team Admin

Endpoin api  e database 

## Prerequisiti

Installare le seguenti applicazioni

```bash
PHP
Composer
```

Creare un applicazione su Azure Active Directory


## Installazione database
Avviare Mysql command Line
```bash

mysql> CREATE DATABASE ms_teams;

mysql> USE ms_teams;

mysql> source path/dump/sql

```


## Configurazione Backend
Modificare il .env per l'accesso all'istanza del database
```bash
DB_HOST=localhost
DB_PORT=3306
DB_DATABASE=ms_teams
DB_USERNAME=root
DB_PASSWORD=rootroot
```


## Avvio

```bash
php -S localhost:8080 -t public
```

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
Uniba