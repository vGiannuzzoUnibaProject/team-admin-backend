<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return "Hello World";
});


$router->post('/login', 'ExampleController@postLogin');
$router->post('/register/access', 'ExampleController@RegisterAccess');
$router->get('/courses/enabled', 'ExampleController@enabled_courses'); 

$router->group(['middleware' => "auth"],function($router){
    $router->post('/register', 'ExampleController@save');
    $router->post('/actionLog', 'ExampleController@insertActionLog');
    $router->get('/listActions/administrator', 'ExampleController@getActionLogAdministrator');

    $router->get('/listActions', 'ExampleController@ActionLog');
    $router->get('/dashboard/access', 'ExampleController@dashboardAccess');
    $router->get('/dashboard/actions', 'ExampleController@dashboardAction');

    $router->get('/user/role', 'ExampleController@userRole');
    $router->put('/user/changerole', 'ExampleController@changeRole');
    $router->put('/user/changepassword', 'ExampleController@changePassword');

    $router->put('/user/enabledisable', 'ExampleController@enableDisable');
    $router->put('/user/resetpassword/id/{id}', 'ExampleController@resetPasswordUser');

    $router->get('/logs/search/mail/{mail}/from/{from}/to/{to}', 'ExampleController@searchLog');
    $router->get('/logs/search/mail/{mail}/from/{from}', 'ExampleController@searchLog');
    $router->get('/logs/search/mail/{mail}/to/{to}', 'ExampleController@searchLog');
    $router->get('/logs/search/mail/{mail}/from/{from}/to/{to}', 'ExampleController@searchLog');
    $router->get('/logs/search/mail/{mail}', 'ExampleController@searchLog');
    $router->get('/logs/search/from/{from}', 'ExampleController@searchLog');
    $router->get('/logs/search/to/{to}', 'ExampleController@searchLog');
    $router->get('/logs/search/from/{from}/to/{to}', 'ExampleController@searchLog');

    $router->get('/logs/time/', 'ExampleController@logsTime');    
    $router->get('/logs', 'ExampleController@logs');    
    $router->get('/logs/limit/{limit}', 'ExampleController@logs');    

    $router->get('/all_username_log', 'ExampleController@all_username_log');    
    $router->get('/all_users', 'ExampleController@all_users');    
    $router->delete('/user/id/{id}', 'ExampleController@deleteUser');    

    //corsi
    $router->get('/courses/all', 'ExampleController@all_courses'); 
    $router->put('/courses/enable_disable', 'ExampleController@enable_disable_courses');    

    $router->put('/courses/id/{id}', 'ExampleController@edit_courses');    
    $router->delete('/courses/id/{id}', 'ExampleController@delete_courses');    
    $router->post('/courses/new', 'ExampleController@new_courses');  
    
    //dettalio_courses
    $router->get('/courses/detail/id/{id}', 'ExampleController@detail_courses'); 
    $router->put('/courses/detail/id/{id}', 'ExampleController@detail_edit_courses');    
    $router->delete('/courses/detail/id/{id}', 'ExampleController@detail_delete_courses');    
    $router->post('/courses/detail/id/{id}', 'ExampleController@detail_new_courses');    






    $router->delete('/logout', 'ExampleController@logout');   
    $router->delete('/logs/delete', 'ExampleController@delete');    
 
});
