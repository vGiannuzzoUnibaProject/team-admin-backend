<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Tymon\JWTAuth\JWTAuth;
use Datetime;

use App\User;
class ExampleController extends Controller
{

    /**
     * @var \Tymon\JWTAuth\JWTAuth
     */
    protected $jwt;

    public function __construct(JWTAuth $jwt)
    {
        $this->jwt = $jwt;
    }
    private function check_request($type,$body){
        if($type=="register"){
            if(!array_key_exists('firstname',$body))
            return false;
            if(!array_key_exists('lastname',$body))
            return false;
            if(!array_key_exists('username',$body))
            return false;
            if(!array_key_exists('role',$body))
            return false;
            if(!array_key_exists('password',$body))
            return false;
           

            return true;

        }

    }

    public function dashboardAccess(){

        $query=DB::select("SELECT DATE_FORMAT(access_time ,'%d-%m-%Y') as access_time,count(*) as count
        from log_access
        group by DATE_FORMAT(access_time ,'%d-%m-%Y')
        order by access_time
        limit 30");


        return response()->json(['result' => $query]);


    }

    
    public function dashboardAction(){

        $query=DB::select("select sum(number_rows) as number,action
        from action_log
        group by action");


        return response()->json(['result' => $query]);


    }
    public function save(Request $request){
        $role=$this->jwt->user()['role'];
        if($role!="admin")
        return response()->json(['error' => 'No admin'], 403, ['X-Header-One' => 'Header Value']);

        $created_by=$this->jwt->user()['username'];
        $body=$request->all();
        $body['created_by']=$created_by;
        if(!$this->check_request('register',$body))
            return response()->json(['error' => 'Params assenti'], 400, ['X-Header-One' => 'Header Value']);

        $query=DB::select("SELECT id FROM users where username= ?",[$body['username']]);
        if(!empty($query))
            return response()->json(['error' => 'Utente esistente'], 400, ['X-Header-One' => 'Header Value']);

        DB::table('users')->insert(
            [
                'first_name' => $body['firstname'],
                'last_name' => $body['lastname'],
                'username' => $body['username'],
                'role' => $body['role'],
                'created_by' => $body['created_by'],
                'enabled' =>1,

                'password' => Hash::make($body['password']),
            ]
        );

        return response()->json(['msg' => 'Updated']);




        }

    public function all_courses(){

        $query=DB::select("SELECT courses.id as id, courses.label as label, enabled,count(detail_courses.id_courses) as number_detail
        from courses
        left join detail_courses
        on courses.id=detail_courses.id_courses
        group by courses.id
		order by label");


        return response()->json(['result' => $query]);
    }

    public function enabled_courses(){

        $query=DB::select("SELECT DISTINCT courses.id,courses.label,enabled
        from courses 
        inner join detail_courses
        on courses.id=detail_courses.id_courses
        where enabled=1 
        order by courses.label");

        foreach($query as $r){
            $id=$r->id;
            $detail_course=DB::select("SELECT id,label,url,color
            from detail_courses
            where id_courses = ?",[$id]);
            $r->detail=$detail_course;


        }


        return response()->json(['result' => $query]);
    }

    public function detail_courses($id){
        $label_courses="";
        $query=DB::select("SELECT detail_courses.id,detail_courses.label,url,color,courses.label as label_courses
        FROM detail_courses
        inner join courses on detail_courses.id_courses=courses.id
        where courses.id = ? order by detail_courses.label ",[$id]);
        foreach($query as $r){
            $label_courses = $r->label_courses;

            break;
        }
        return response()->json(['result' => $query,'courses' => $label_courses]);
    }

    public function detail_edit_courses(Request $request,$id){
        $role=$this->jwt->user()['role'];

        if($role!="admin")
            return response()->json(['error' => 'No admin'], 403, ['X-Header-One' => 'Header Value']);

        $label=$request->input('label');
        $url= $request->input('url');
        $color= $request->input('color');

        DB::update("UPDATE detail_courses set label=?,url=?,color=?
        where id =?",[$label,$url,$color,$id]);
            return response()->json(['msg' => "Updated Ok"]);

    }

    public function detail_delete_courses(Request $request,$id){
        $role=$this->jwt->user()['role'];

        if($role!="admin")
            return response()->json(['error' => 'No admin'], 403, ['X-Header-One' => 'Header Value']);


        DB::delete("DELETE from detail_courses 
        where id =?",[$id]);
            return response()->json(['msg' => "Delete Ok"]);

    }

    public function detail_new_courses(Request $request,$id){
        $role=$this->jwt->user()['role'];

        if($role!="admin")
            return response()->json(['error' => 'No admin'], 403, ['X-Header-One' => 'Header Value']);

        $label=$request->input('label');
        $url= $request->input('url');
        $color= $request->input('color');

        DB::insert("INSERT INTO detail_courses (label,url,id_courses,color) values(?,?,?,?)",
        [$label,$url,$id,$color]);
        return response()->json(['msg' => "Insert Ok"]);

    }
    public function enable_disable_courses(Request $request){
        $role=$this->jwt->user()['role'];

        if($role!="admin")
            return response()->json(['error' => 'No admin'], 403, ['X-Header-One' => 'Header Value']);
        $body=$request->all();
        foreach ($body as $key => $value) {
        if($value==TRUE)
            $value=1;
        else $value=0;
        DB::update("UPDATE courses set enabled = ? where id= ?",[$value,$key]);

    }
    return response()->json(['msg' => "updated"]);
    }

    public function new_courses(Request $request){
        $role=$this->jwt->user()['role'];

        if($role!="admin")
            return response()->json(['error' => 'No admin'], 403, ['X-Header-One' => 'Header Value']);

        $label=$request->input('label');

        DB::insert("INSERT INTO courses (label,enabled) values(?,1)",[$label]);
        return response()->json(['msg' => "Insert Ok"]);

    }


    public function delete_courses($id){
        $role=$this->jwt->user()['role'];

        if($role!="admin")
            return response()->json(['error' => 'No admin'], 403, ['X-Header-One' => 'Header Value']);

        DB::delete("DELETE FROM courses where id =?",[$id]);
            return response()->json(['msg' => "Delete Ok"]);
    
    }
    public function edit_courses($id,Request $request){
        $role=$this->jwt->user()['role'];

        $label=$request->input('label');
        if($role!="admin")
            return response()->json(['error' => 'No admin'], 403, ['X-Header-One' => 'Header Value']);

        DB::update("UPDATE courses set label=? where id =?",[$label,$id]);
            return response()->json(['msg' => "Updated Ok"]);
    
    }

        
        
/*
        DB::table('users')->insert(
            [
                'email' => 'sam@mail.com',
                'password' => Hash::make("sam1"),
            ]
        );

        */
    
     private function convertDate($date){
        $new_date = explode("-", $date);
        return $new_date[2].'-'.$new_date[1].'-'.$new_date[0];
    }

    private function addWhere($where,$condition){
         if($where)
         return $where." AND ".$condition;
         else return $condition;
    }
    public function logout(){

        $this->jwt->parseToken()->invalidate();
        return response()->json(['msg' => 'logout']);
    }
    public function searchLog($mail=null,$from=null,$to=null){
    
       $username=$mail;
       $from=$from;
       $to=$to;
       $query=("SELECT DATE_FORMAT(access_time ,'%d-%m-%Y') AS data_access,
       TIME_FORMAT(access_time, '%H:%i:%s') as hour_access,
       mail,
       public_ip,
       displayName,
       log_access.id_user,
       log_access.id,
       log_access.id as 'key'
       FROM log_access
       INNER JOIN microsoft_users
       ON log_access.id_user=microsoft_users.id WHERE ");
       if($username || $to || $from){
           $strWhere="";
           if($username){
            $username=str_replace("$",".",$username);
            $strWhere=$this->addWhere($strWhere,"mail = '".$username."'");
           }
           if($from){
            $from=$from/1000;
            $strWhere=$this->addWhere($strWhere,"date_format(access_time,'%Y-%m-%d') >=
            FROM_UNIXTIME($from,'%Y-%m-%d')");
        }
        if($to){
            $to=$to/1000;

            $strWhere=$this->addWhere($strWhere,"date_format(access_time,'%Y-%m-%d') <= 
            FROM_UNIXTIME($to,'%Y-%m-%d')");
        }
        $results = DB::select($query.$strWhere."
        order by access_time desc" );
        return response()->json(['result' => $results]);

       }

      else
       return response()->json(['No filter'], 400);

       // $query= DB::select("SELECT distinct username as value from users order by username");
       
        //return response()->json(['result' => $query]);
 
     }


     public function ActionLog(){
        $query= DB::select("SELECT DATE_FORMAT(date_action ,'%d-%m-%Y') AS data_access,
        TIME_FORMAT(date_action, '%H:%i:%s') as hour_access,action,number_rows,username
        from action_log
        inner join users
        on action_log.id_users=users.id
        order by date_action desc" );
       return response()->json(['result' => $query]);


     }
     public function ChangePassword(Request $request){


        $oldPassword=$request->input('oldPassword');
        $newPassword= $request->input('newPassword');
        $username=$this->jwt->user()['username'];
       // echo Hash::check($oldPassword);
       $querycheck=DB::select("SELECT password from users where username=?",[$username]);
        //return response()->json(['error' => 'Password non coincidenti'], 400, ['X-Header-One' => 'Header Value']);
        foreach($querycheck as $r){
            $password= $r->password;
        };

        if(Hash::check($oldPassword,$password)){
        DB::UPDATE("UPDATE users SET password=? where username =?",
            [Hash::make($newPassword),$username]);
        return response()->json(['msg' => "Change Password Ok"]);
        }
        else 
        return response()->json(['error' => 'Password non coincidenti'], 400, ['X-Header-One' => 'Header Value']);

        

       


     }


    public function all_username_log(){
       $query= DB::select("SELECT distinct microsoft_users.mail as value from 
       log_access
       INNER JOIN microsoft_users
        on  log_access.id_user = microsoft_users.id
               order by mail");
      
       return response()->json(['result' => $query]);

    }

    public function all_users(){

        $query= DB::select("
        SELECT distinct first_name,username,last_name,id,role,created_by,enabled
       from users order by id");
      
       return response()->json(['result' => $query]);

    }
    public function logs($limit=null){

        if ($limit)
            $strWhere="limit ".$limit;
        else $strWhere="";
        $results = DB::select
        ("SELECT DATE_FORMAT(access_time ,'%d-%m-%Y') AS data_access,
        TIME_FORMAT(access_time, '%H:%i:%s') as hour_access,
        mail,
        public_ip,
        displayName,
        log_access.id,
        log_access.id_user,
        log_access.id as 'key'
        FROM log_access
        INNER JOIN microsoft_users
        ON log_access.id_user=microsoft_users.id
        order by access_time desc ".$strWhere);
    return response()->json(['result' => $results]);
        
    }

    public function userRole()
    {

        $role=$this->jwt->user()['role'];
        return response()->json(['role' => $role]);

    }
    public function deleteUser($id){
        $role=$this->jwt->user()['role'];

        if($role!="admin")
        return response()->json(['error' => 'No admin'], 403, ['X-Header-One' => 'Header Value']);

        DB::delete("DELETE FROM users where id =?  and username!='admin'",[$id]);
        return response()->json(['msg' => "Delete Ok"]);

        }

   

        public function resetPasswordUser($id,Request $request){
            $role=$this->jwt->user()['role'];
            $password= $request->input('password');

            if($role!="admin")
            return response()->json(['error' => 'No admin'], 403, ['X-Header-One' => 'Header Value']);
            
            DB::UPDATE("UPDATE users SET password=? where id =? and username !='admin'",
            [Hash::make($password),$id]);
            return response()->json(['msg' => "Reset Password Ok"]);
    
            }



    
    public function delete(Request $request){
        $role=$this->jwt->user()['role'];
        if($role!="admin")
        return response()->json(['error' => 'No admin'], 403, ['X-Header-One' => 'Header Value']);
        else {
        $body=$request->all();
        foreach ($body as &$value) {
            str_replace(" ","",$value['time']);
            $date=$value['date']." ".$value['time'];
            DB::delete("DELETE FROM log_access where id =? and access_time = ?"
        ,[$value['id'],$date]);
        }

        return response()->json(['msg' => "Delete Ok"]);

        }}

    public function changeRole(Request $request){
        $role=$this->jwt->user()['role'];
        if($role!="admin")
        return response()->json(['error' => 'No admin'], 403, ['X-Header-One' => 'Header Value']);
        $id= $request->input('id');
        $value= $request->input('value');
        if($value!='admin' && $value!='viewer')
            return response()->json(['error' => 'No role'], 400, ['X-Header-One' => 'Header Value']);
        $username_from=$this->jwt->user()['username'];
        DB::update("UPDATE users set role = ? where id= ? and username !='admin'",[$value,$id]);
        
        $action="switch_".$value;
        DB::INSERT("INSERT INTO actions_administrator (data_access,from_username,to_id,action)values (NOW()+1,?,?,?)"
        ,[$username_from,$id,$action]);
        
        
        return response()->json(['msg' => "updated"]);

    }

    public function enableDisable(Request $request){
        $role=$this->jwt->user()['role'];
        if($role!="admin")
        return response()->json(['error' => 'No admin'], 403, ['X-Header-One' => 'Header Value']);
        $body=$request->all();
        foreach ($body as $key => $value) {
            if($value==TRUE)
                $value=1;
            else $value=0;
            DB::update("UPDATE users set enabled = ? where id= ? and username !='admin'",[$value,$key]);


        
        }
        return response()->json(['msg' => "updated"]);

    }

    public function getActionLogAdministrator(Request $request){

        $role=$this->jwt->user()['role'];
        if($role!="admin")
        return response()->json(['error' => 'No admin'], 403, ['X-Header-One' => 'Header Value']);

        $results = DB::select("SELECT DATE_FORMAT(data_access ,'%d-%m-%Y') as data,
        TIME_FORMAT(data_access, '%H:%i:%s') as hour_access,actions_administrator.id, from_username,users.username as to_username,action FROM actions_administrator 
  
        inner join users on users.id=actions_administrator.to_id  order by data_access desc");
        return response()->json(['result' => $results]);


    }
    public function RegisterAccess(Request $request){
        //GET PER INSERIRE L'UTENTE
        $idMicrosoft= $request->input('id');
        $public_ip= $request->input('ip_login');
      
        $results = DB::select("SELECT * FROM microsoft_users where idMicrosoft =? "
        ,[$idMicrosoft]);
        if (!($results)){ 
            $displayName=$request->input('displayName');
            $givenName=$request->input('givenName');
            $mail=$request->input('mail');
            $jobTitle=$request->input('jobTitle');
            $surname=$request->input('surname');
            $userPrincipalName=$request->input('userPrincipalName');

            DB::insert("INSERT INTO microsoft_users (idMicrosoft,displayName,mail,jobTitle,surname,userPrincipalName) VALUES (?,?,?,?,?,?)",
            [$idMicrosoft,$displayName,$mail,$jobTitle,$surname,$userPrincipalName]);
            
        }

        $results = DB::select("SELECT id FROM microsoft_users where idMicrosoft =? limit 1",[$idMicrosoft]);
        foreach($results as $r){
            $id_user= $r->id;
        };
        $date = new DateTime();
        $date=$date->getTimestamp();
       // $date=date("Y-m-d H:i:s.u",$date);
     
        DB::insert("INSERT INTO log_access (id_user,access_time,public_ip) 
        VALUES (?,NOW()+1,?)",[$id_user,$public_ip]);
        
        return response()->json(['msg' => $date]);


    }
    public function insertActionLog(Request $request){

        $id=$this->jwt->user()['id'];
      
        $type= $request->input('type');
        $number= $request->input('number');

        DB::insert("INSERT INTO action_log (date_action,action,id_users,number_rows) 
        VALUES (NOW() + 1,?,?,?)",[$type,$id,$number]);
        
        return response()->json(['msg' => "Insert log"]);
    }
    public function postLogin(Request $request)
    {
        $this->validate($request, [
            'username'    => 'required|max:255',
            'password' => 'required',
        ]);

        try {

            if (! $token = $this->jwt->attempt($request->only('username', 'password'))) {
                return response()->json(['user_not_found'], 404);
            }

        } catch (\Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

            return response()->json(['token_expired'], 500);

        } catch (\Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {

            return response()->json(['token_invalid'], 500);

        } catch (\Tymon\JWTAuth\Exceptions\JWTException $e) {

            return response()->json(['token_absent' => $e->getMessage()], 500);

        }

        if( $this->jwt->user()['enabled'])
            return response()->json(compact('token'));
        else  return response()->json(['no_priv'], 403);

    }
}
