-- MySQL dump 10.13  Distrib 8.0.22, for Win64 (x86_64)
--
-- Host: localhost    Database: ms_teams
-- ------------------------------------------------------
-- Server version	8.0.22

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `action_log`
--

DROP TABLE IF EXISTS `action_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `action_log` (
  `id` int NOT NULL AUTO_INCREMENT,
  `action` varchar(45) NOT NULL,
  `id_users` int DEFAULT NULL,
  `number_rows` varchar(45) DEFAULT NULL,
  `date_action` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_users_idx` (`id_users`),
  CONSTRAINT `id_users` FOREIGN KEY (`id_users`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `action_log`
--

LOCK TABLES `action_log` WRITE;
/*!40000 ALTER TABLE `action_log` DISABLE KEYS */;
INSERT INTO `action_log` VALUES (36,'delete',1,'1','2021-01-06 11:20:31'),(37,'delete',1,'1','2021-01-08 16:41:15'),(38,'delete',1,'1','2021-01-08 16:41:23'),(39,'delete',1,'1','2021-01-08 16:41:41'),(40,'delete',1,'1','2021-01-08 16:49:22'),(41,'delete',1,'1','2021-01-08 16:49:31'),(42,'delete',1,'1','2021-01-08 16:49:52'),(43,'delete',1,'27','2021-01-08 17:00:26'),(44,'delete',22,'1','2021-01-09 09:08:17'),(45,'delete',22,'1','2021-01-09 09:08:57'),(46,'delete',22,'1','2021-01-09 09:10:29'),(47,'delete',22,'2','2021-01-09 09:12:28'),(48,'delete',1,'2','2021-01-09 09:12:49'),(49,'delete',1,'2','2021-01-09 14:57:48');
/*!40000 ALTER TABLE `action_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `actions_administrator`
--

DROP TABLE IF EXISTS `actions_administrator`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `actions_administrator` (
  `id` int NOT NULL AUTO_INCREMENT,
  `from_username` varchar(45) NOT NULL,
  `to_id` varchar(45) NOT NULL,
  `action` varchar(45) NOT NULL,
  `data_access` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `actions_administrator`
--

LOCK TABLES `actions_administrator` WRITE;
/*!40000 ALTER TABLE `actions_administrator` DISABLE KEYS */;
/*!40000 ALTER TABLE `actions_administrator` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `courses`
--

DROP TABLE IF EXISTS `courses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `courses` (
  `id` int NOT NULL AUTO_INCREMENT,
  `label` varchar(255) NOT NULL,
  `enabled` tinyint NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `courses`
--

LOCK TABLES `courses` WRITE;
/*!40000 ALTER TABLE `courses` DISABLE KEYS */;
INSERT INTO `courses` VALUES (15,'Corso di Grafica',1);
/*!40000 ALTER TABLE `courses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `detail_courses`
--

DROP TABLE IF EXISTS `detail_courses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `detail_courses` (
  `id` int NOT NULL AUTO_INCREMENT,
  `id_courses` int NOT NULL,
  `label` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `color` varchar(20) NOT NULL DEFAULT '#2196f3',
  PRIMARY KEY (`id`),
  KEY `id_courses_idx` (`id_courses`),
  CONSTRAINT `id_courses` FOREIGN KEY (`id_courses`) REFERENCES `courses` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `detail_courses`
--

LOCK TABLES `detail_courses` WRITE;
/*!40000 ALTER TABLE `detail_courses` DISABLE KEYS */;
INSERT INTO `detail_courses` VALUES (7,15,'Corso Photoshop','https://www.udemy.it','#f57c00');
/*!40000 ALTER TABLE `detail_courses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `log_access`
--

DROP TABLE IF EXISTS `log_access`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `log_access` (
  `id` int NOT NULL AUTO_INCREMENT,
  `id_user` int unsigned DEFAULT NULL,
  `access_time` datetime NOT NULL,
  `public_ip` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_user` (`id_user`) /*!80000 INVISIBLE */
) ENGINE=InnoDB AUTO_INCREMENT=116 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `log_access`
--

LOCK TABLES `log_access` WRITE;
/*!40000 ALTER TABLE `log_access` DISABLE KEYS */;
INSERT INTO `log_access` VALUES (115,3,'2021-01-09 14:57:26','37.179.169.170');
/*!40000 ALTER TABLE `log_access` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `microsoft_users`
--

DROP TABLE IF EXISTS `microsoft_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `microsoft_users` (
  `idMicrosoft` varchar(100) NOT NULL,
  `displayName` varchar(100) DEFAULT NULL,
  `givenName` varchar(100) DEFAULT NULL,
  `mail` varchar(100) NOT NULL,
  `jobTitle` varchar(100) DEFAULT NULL,
  `surname` varchar(100) DEFAULT NULL,
  `userPrincipalName` varchar(100) DEFAULT NULL,
  `id` int NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `microsoft_users`
--

LOCK TABLES `microsoft_users` WRITE;
/*!40000 ALTER TABLE `microsoft_users` DISABLE KEYS */;
INSERT INTO `microsoft_users` VALUES ('32921849-96fe-4e60-ad6b-3b32bda67d2d','Vincenzo Giannuzzo',NULL,'v.giannuzzo@studenti.uniba.it',NULL,'Giannuzzo','v.giannuzzo@studenti.uniba.it',3);
/*!40000 ALTER TABLE `microsoft_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
  `first_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `username` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `role` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id` int NOT NULL AUTO_INCREMENT,
  `created_by` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `enabled` tinyint DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES ('admin','admin','admin','$2y$10$0tesc6LGPSTcmcbqxl2ma.BzQVCTgu.yRNJ8.MiqimFzJHSeZAxwO','admin',1,'SYSTEM',1),('admin2','admin2','admin2','$2y$10$KVcaD/05t4SxuPAWHstFzOwmvMxcFZdqKXeQfwCwDw9E45W1GzNKW','viewer',22,'admin',1);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-01-12 20:50:06
